from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen,Key,KeyChord
from libqtile.lazy import lazy
from libqtile.widget import base
from libqtile.utils import guess_terminal
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from typing import List

mod = "mod4"
terminal = guess_terminal()

keys = [
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    #Custom Spawn Controls
    Key([mod], "F1", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "F2", lazy.spawn("firefox"), desc="open firefox"),
    Key([mod], "F3", lazy.spawn([terminal, "-e", "nvim"]), desc="opens neovim"),
    Key([mod], "F4", lazy.spawn("code"), desc="open vscode aka CodeOSS"),
    Key([mod], "F5", lazy.spawn("rofi -show drun"), desc="opens rofi"),
    Key([mod], "F6", lazy.spawn("rofi -modi emoji -show emoji"), desc="opens rofi"),



    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    # Sound
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pamixer -i 5"), desc='Volume Up'),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pamixer -d 5"), desc="Lower Volume by 1%"),
    Key([], "XF86AudioMute", lazy.spawn("pamixer -t"), desc="Mute/Unmute Volume"),
    Key([mod], "Page_up", lazy.spawn("pamixer -i 1"), desc='Volume Up'),
    Key([mod], "Page_down", lazy.spawn("pamixer -d 1"), desc="Lower Volume by 1%"),
    Key([mod], "End", lazy.spawn("pamixer -t"), desc="Mute/Unmute Volume"),
    # Mod keys
    Key([], "ISO_Level3_Shift", lazy.function(lambda: None)),
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
       ]
    )

layouts = [
        layout.Columns(border_focus_stack=["#FF00FF", "#FF00FF"], border_width=5, border_focus="#DA70D6", border_normal="#4C4E52"),
    layout.Max(),
]

widget_defaults = dict(
    font="sans",
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()
   # Colors 
colors =  [
        ["#1b1c26", "#14151C", "#1b1c26"], # color 0
        ["#485062", "#485062", "#485062"], # color 1
        ["#65bdd8", "#65bdd8", "#65bdd8"], # color 2
        ["#bc7cf7", "#a269cf", "#bc7cf7"], # color 3
        ["#aed1dc", "#98B7C0", "#aed1dc"], # color 4
        ["#ffffff", "#ffffff", "#ffffff"], # color 5
        ["#bb94cc", "#AB87BB", "#bb94cc"], # color 6
        ["#9859B3", "#8455A8", "#9859B3"], # color 7
        ["#744B94", "#694486", "#744B94"], # color 8
        ["#0ee9af", "#0ee9af", "#0ee9af"]]
newColors = [
        ["#191970"], # Color 0
        ["#C3B1E1","#C3B1E1","#C3B1E1"], # Color 1 
        ["#763fbc"], # Color 2 
        ["#43236e"], # Color 3
        ]
   # Qtile Bar 
screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentLayout(),
                widget.GroupBox(),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.TextBox("default config", name="default"),
                widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                # NB Systray is incompatible with Wayland, consider using StatusNotifier instead
                # widget.StatusNotifier(),
                widget.Systray(),
                widget.Clock(format="%Y-%m-%d %a %I:%M %p"),
                widget.QuickExit(),
            ],
            24,
        ),
    ),
]

screens = [
    Screen(
	wallpaper='~/Pictures/city.jpg',
	wallpaper_mode='stretch',
        bottom=bar.Bar(
        [
            widget.Sep(
                background=newColors[1],
                padding=15,
                linewidth=0,
            ),
            widget.Clock(
                font="sans",
                fontsize=14,
                foreground=colors[5],
                background=newColors[1],
                format='%d %b | %A'
            ),
            widget.TextBox(
                text="\ue0b4",
                fonts="sans",
                foreground=newColors[1],
                background=newColors[2],
                padding=0,
                fontsize=33
            ),
            widget.Sep(
                background=newColors[2],
                padding=12,
                linewidth=0,
            ),
            widget.CurrentLayout(
                background=newColors[2],
                foreground=colors[5],
                font="sans",
                fontsize=13,
            ),
            widget.TextBox(
                text="\ue0b4",
                fonts="sans",
                foreground=newColors[2],
                background=newColors[3],
                padding=0,
                fontsize=33
            ),
            widget.TextBox(
                text=" ",
                foreground=colors[5],
                background=newColors[3],
                padding=0,
                fontsize=33
            ),
            widget.Memory(
                background=newColors[3],
                foreground=colors[5],
                font="sans",
                fontsize=14,
                format='{MemUsed: .0f} MB',
            ),
            widget.TextBox(
                text="\ue0b4",
                fonts="sans",
                background="#848884",
                foreground=newColors[3],
                padding=0,
                fontsize=33
            ),
            widget.Spacer(
                background="#848884"
            ),
            widget.GroupBox(
                font="sans",
                fontsize=10,
                background="#848884",
                active=colors[6],
                inactive=colors[1],
                rounded=True,
                highlight_color=colors[5],
                highlight_method="line",
                this_current_screen_border=colors[9],
                block_highlight_text_color=colors[0],
                blockwidth=2,
                margin_y=5,
            ),
            
            widget.Spacer(
                background="#848884"
            ),

            widget.TextBox(
                text="\uE0B6",
                fonts="sans",
                background="#848884",
                foreground=newColors[3],
                padding=0,
                fontsize=33
            ),
            widget.TextBox(
                text="",
                foreground=colors[5],
                background=newColors[3],
                padding=0,
                fontsize=33
            ),
            widget.CPU(
                background=newColors[3],
                foreground=colors[5],
                format=' {load_percent}% |',
                font='sans',
                fontsize=14
            ),
            widget.TextBox(
                text="\uE0B6",
                fonts="sans",
                foreground=newColors[3],
                background=newColors[3],
                padding=0,
                fontsize=33
            ),
             widget.Sep(
                background=newColors[3],
                padding=10,
                linewidth=0,
            ),
            widget.TextBox(
                text="\uE0B6",
                fonts="sans",
                foreground=newColors[2],
                background=newColors[3],
                padding=0,
                fontsize=33
            ),
            widget.Sep(
                background=newColors[2],
                padding=6,
                linewidth=0,
            ),
            widget.Battery(
                foreground=colors[5],
                background=newColors[2],
                fontsize=17,
                low_percentage=0.2,
                low_foreground=colors[5],
                font="sans",
                update_interval=1,
                format='{char}',
                charge_char='',
                discharge_char='',
            ),
            widget.Battery(
                background=newColors[2],
                foreground=colors[5],
                charge_char='↑',
                discharge_char='↓',
                font="sans",
                fontsize=14,
                update_interval=1,
                format='{percent:2.0%}'
            ),
            widget.Sep(
                background=newColors[2],
                padding=6,
                linewidth=0,
            ),
            widget.TextBox(
                text="\uE0B6",
                fonts="sans",
                foreground=newColors[1],
                background=newColors[2],
                padding=0,
                fontsize=33
            ),
            widget.Sep(
                background=newColors[1],
                padding=6,
                linewidth=0,
            ),
            widget.Clock(
                background=newColors[1],
                foreground=colors[5],
                font="sans",
                fontsize=15,
                format='%I:%M %p',
            ),
            widget.Sep(
                background=newColors[1],
                padding=10,
                linewidth=0,
            ),
        ],
            37,
            background=colors[0],
            # margin=[0,8,8,8],
            # opacity=0.8,
        ),
    ),
    Screen(
	wallpaper='~/Pictures/tree.jpg',
	wallpaper_mode='stretch',
        bottom=bar.Bar(
        [
            widget.Sep(
                background=colors[2],
                padding=15,
                linewidth=0,
            ),
            widget.Clock(
                font="sans",
                fontsize=14,
                foreground=colors[5],
                background=colors[2],
                format='%d %b | %A'
            ),
            widget.TextBox(
                text="\ue0b4",
                fonts="sans",
                foreground=colors[2],
                background=colors[9],
                padding=0,
                fontsize=33
            ),
            widget.Sep(
                background=colors[9],
                padding=12,
                linewidth=0,
            ),
            widget.CurrentLayout(
                background=colors[9],
                foreground=colors[5],
                font="sans",
                fontsize=13,
            ),
            widget.TextBox(
                text="\ue0b4",
                fonts="sans",
                foreground=colors[9],
                background=newColors[0],
                padding=0,
                fontsize=33
            ),
            widget.TextBox(
                text=" ",
                foreground=colors[5],
                background=newColors[0],
                padding=0,
                fontsize=33
            ),
            widget.Memory(
                background=newColors[0],
                foreground=colors[5],
                font="sans",
                fontsize=14,
                format='{MemUsed: .0f} MB',
            ),
            widget.TextBox(
                text="\ue0b4",
                fonts="sans",
                background="#848884",
                foreground=newColors[0],
                padding=0,
                fontsize=33
            ),
            widget.Spacer(
                background="#848884"
            ),
            widget.GroupBox(
                font="sans",
                fontsize=10,
                background="#848884",
                active=colors[6],
                inactive=colors[1],
                rounded=True,
                highlight_color=colors[5],
                highlight_method="line",
                this_current_screen_border=colors[9],
                block_highlight_text_color=colors[0],
                blockwidth=2,
                margin_y=5,
            ),
            
            widget.Spacer(
                background="#848884"
            ),

            widget.TextBox(
                text="\uE0B6",
                fonts="sans",
                background="#848884",
                foreground=colors[2],
                padding=0,
                fontsize=33
            ),
            widget.TextBox(
                text="",
                foreground=colors[5],
                background=colors[2],
                padding=0,
                fontsize=33
            ),
            widget.CPU(
                background=colors[2],
                foreground=colors[5],
                format=' {load_percent}% |',
                font='sans',
                fontsize=14
            ),
            widget.TextBox(
                text="\uE0B6",
                fonts="sans",
                foreground=colors[2],
                background=colors[2],
                padding=0,
                fontsize=33
            ),
             widget.Sep(
                background=colors[2],
                padding=10,
                linewidth=0,
            ),
            widget.TextBox(
                text="\uE0B6",
                fonts="sans",
                foreground=colors[9],
                background=colors[2],
                padding=0,
                fontsize=33
            ),
            widget.Sep(
                background=colors[9],
                padding=6,
                linewidth=0,
            ),
            widget.Battery(
                foreground=colors[5],
                background=colors[9],
                fontsize=17,
                low_percentage=0.2,
                low_foreground=colors[5],
                font="sans",
                update_interval=1,
                format='{char}',
                charge_char='',
                discharge_char='',
            ),
            widget.Battery(
                background=colors[9],
                foreground=colors[5],
                charge_char='↑',
                discharge_char='↓',
                font="sans",
                fontsize=14,
                update_interval=1,
                format='{percent:2.0%}'
            ),
            widget.Sep(
                background=colors[9],
                padding=6,
                linewidth=0,
            ),
            widget.TextBox(
                text="\uE0B6",
                fonts="sans",
                foreground=newColors[0],
                background=colors[9],
                padding=0,
                fontsize=33
            ),
            widget.Sep(
                background=newColors[0],
                padding=6,
                linewidth=0,
            ),
            widget.Clock(
                background=newColors[0],
                foreground=colors[5],
                font="sans",
                fontsize=15,
                format='%I:%M %p',
            ),
            widget.Sep(
                background=newColors[0],
                padding=10,
                linewidth=0,
            ),
        ],
            37,
            background=colors[0],
            # margin=[0,8,8,8],
            # opacity=0.8,
        ),
    )
]
# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

auto_minimize = True

wl_input_rules = None

wmname = "LG3D"
